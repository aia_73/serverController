//
//  MessageResponse.swift
//  staffDirectory
//
//  Created by alex borisoft on 10.05.17.
//
//

import Foundation
import PerfectLib

class MessageResponse : JSONConvertibleObject {
    
    static let registerName = "messageResponse"
    
    var code: String = ""
    var responseDescription: String = ""
    var dataType: String = ""
    var data: String = ""
    
    init(code: String, responseDescription: String, dataType: String, data: String) {
        self.code = code
        self.responseDescription = responseDescription
        self.dataType = dataType
        self.data = data
    }
    
    override public func setJSONValues(_ values: [String : Any]) {
        self.code = getJSONValue(named: "code", from: values, defaultValue: "")
        self.responseDescription = getJSONValue(named: "responseDescription", from: values, defaultValue: "")
        self.dataType = getJSONValue(named: "dataType", from: values, defaultValue: "")
        self.data = getJSONValue(named: "data", from: values, defaultValue: "")
    }
    
    override public func getJSONValues() -> [String : Any] {
        return [
            "code":code,
            "responseDescription":responseDescription,
            "dataType":dataType,
            "data":data
        ]
    }
    
}
