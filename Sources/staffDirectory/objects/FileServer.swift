//
//  FileServer.swift
//  staffDirectory
//
//  Created by alex borisoft on 01.05.17.
//
//

import Foundation
import PerfectLib
import SQLiteStORM
import StORM

/// A sample comment for API docs
class FileServer: SQLiteStORM {
    
    public var id = 0
    
    public var fileUrl = ""
    
    public var token = ""
    
    public var mime = ""
    
    // ORM helper method
    override public func to(_ this: StORMRow) {
        id = this.data["id"] as? Int ?? 0
        fileUrl = this.data["fileUrl"] as? String ?? ""
        token = this.data["token"] as? String ?? ""
        mime = this.data["mime"] as? String ?? ""

    }
    
    // ORM helper method
    func rows() -> [FileServer] {
        var rows = [FileServer]()
        for i in 0..<self.results.rows.count {
            let row = FileServer()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    
    public func fromDict(_ this: [String: Any]) {
        fileUrl = this["fileUrl"] as? String ?? ""
        token = this["token"] as? String ?? ""
        mime = this["mime"] as? String ?? ""
    }
}
