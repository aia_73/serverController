//
//  MessageDB.swift
//  staffDirectory
//
//  Created by alex borisoft on 22.05.17.
//
//

//
//  Message.swift
//  staffDirectory
//
//  Created by alex borisoft on 22.05.17.
//
//

import PerfectLib
import SQLiteStORM
import StORM

/// A sample comment for API docs
class MessageDB: SQLiteStORM {
    
    public var id = 0
    public var token = ""
    public var indexMessage = 0
    public var fromUser = ""
    public var data = ""
    public var dataType = ""
    
    
    override public func to(_ this: StORMRow) {
        id = this.data["id"] as? Int ?? 0
        token = this.data["token"] as? String ?? ""
        indexMessage = this.data["indexMessage"] as? Int ?? 0
        fromUser = this.data["fromUser"] as? String ?? ""
        data = this.data["data"] as? String ?? ""
        dataType = this.data["dataType"] as? String ?? ""
    }
    
    // ORM helper method
    func rows() -> [MessageDB] {
        var rows = [MessageDB]()
        for i in 0..<self.results.rows.count {
            let row = MessageDB()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    
    public func fromDict(_ this: [String: Any]) {
        token = this["token"] as? String ?? ""
        indexMessage = this["indexMessage"] as? Int ?? 0
        fromUser = this["fromUser"] as? String ?? ""
        data = this["data"] as? String ?? ""
        dataType = this["dataType"] as? String ?? ""
    }
}
