//
//  User.swift
//  staffDirectory
//
//  Created by alex borisoft on 30.04.17.
//
//

import Foundation
import PerfectLib
import SQLiteStORM
import StORM

/// A sample comment for API docs
class User: SQLiteStORM {
    
    public var id = 0
    
    public var login = ""
    
    public var password = ""
    
    public var token = ""

    // ORM helper method
    override public func to(_ this: StORMRow) {
        id = this.data["id"] as? Int ?? 0
        login = this.data["login"] as? String ?? ""
        password = this.data["password"] as? String ?? ""
        token = this.data["token"] as? String ?? ""

    }
    
    // ORM helper method
    func rows() -> [User] {
        var rows = [User]()
        for i in 0..<self.results.rows.count {
            let row = User()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    
    public func fromDict(_ this: [String: Any]) {
        login = this["login"] as? String ?? ""
        password = this["password"] as? String ?? ""
        token = this["token"] as? String ?? ""
    }
}
