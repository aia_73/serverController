//
//  Routes.swift
//  staffDirectory
//
//  Created by Jonathan Guthrie on 2017-02-20.
//  Copyright (C) 2017 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//
//  Modified by Clingon: https://github.com/iamjono/clingon
//
import PerfectHTTPServer
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import PerfectMustache

func mainRoutes() -> [[String: Any]] {
    var routes: [[String: Any]] = [[String: Any]]()
    routes.append(["method":"get", "uri":"/**", "handler":PerfectHTTPServer.HTTPHandler.staticFiles, "documentRoot":pathConst,"allowResponseFilters":true])
    
    routes.append(["method":"post", "uri":"/uploadfile", "handler":Handlers.uploadfile])
    
    routes.append(["method":"post", "uri":"/login", "handler":Handlers.userLogin])
    
    routes.append(["method":"post", "uri":"/logout", "handler":Handlers.userLogin])
    
    routes.append(["method":"post", "uri":"/chat", "handler":Handlers.message])
    
    routes.append(["method":"post", "uri":"/last_messages", "handler":Handlers.lastMessage])


    
    return routes
}

