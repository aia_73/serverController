//
//  FileReader.swift
//  staffDirectory
//
//  Created by alex borisoft on 10.05.17.
//
//

import Foundation


class FileReader {
    
    func readFile(file:String ) ->String {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //reading
            do {
               return  try String(contentsOf: path, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
        }
        return ""
    }
    
}
