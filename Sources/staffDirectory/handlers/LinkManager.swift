//
//  LinkManager.swift
//  staffDirectory
//
//  Created by alex borisoft on 04.05.17.
//
//

import Foundation
import PerfectHTTP

class FileLinkManager {
    
    class func createLink( url: String, token: String,  mime: String) {
        
        var link = FileServer()
        
        do {
            try? link.find([("token" , token), ("fileUrl", url)])
            if link.rows().count > 0  {
                return
            }
            
            link = FileServer()
            
            link.fileUrl = url
            link.token = token
            link.mime = mime
            
            try link.save()
        } catch {
            print( "error Creaate Link")
        }
    }
    
    class func  getLinks(domen: String,  token: String) -> [String] {
        let  link  = FileServer()
        do {
            try link.select(columns: [], whereclause: "token = :1", params: [token], orderby: [])

            var data = [String]()
            for obj in link.rows() {
                data.append(obj.fileUrl)
            }
            
            return data
        } catch {
            print("error getLinks")
        }
        
        return []
    }
    
    class func  drop(domen: String, token:String) -> [String] {
        let  link  = FileServer()
        do {
            try? link.findAll()
            
            link.rows().dropFirst(link.rows().count)
        } catch {
            print("error getLinks")
        }
        
        return []
    }
}

