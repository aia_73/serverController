
//  Handlers.swift


import PerfectHTTP
import StORM
import Foundation
import PerfectMustache
import PerfectLib


class Handlers {
    
    static func main(data: [String:Any]) throws -> RequestHandler {
        return {
            request, response in
            response.setBody(string: "<html><title>Hello, world!</title><body>Hello, world!</body></html>")
            response.completed()
        }
    }
    
    
    
    static func shutdown(data: [String:Any]) throws ->RequestHandler {
        
        return {
            request, response in
            
            response.setHeader(.contentType, value: "application/json")
            response.appendBody(string: "{\"message\":\"Mac os выключается\"}")
            response.completed()
            
            let source = "tell application \"Finder\"\nshut down\nend tell"
            
            let script = NSAppleScript(source: source)
            script?.executeAndReturnError(nil)
        }
    }
    
    static func sleep(data: [String:Any]) throws ->RequestHandler {
        
        return {
            request, response in
            
            response.setHeader(.contentType, value: "application/json")
            response.appendBody(string: "{\"message\":\"Mac os засыпает\"}")
            response.completed()
            
            let source = "tell application \"System Events\" to sleep"
            
            let script = NSAppleScript(source: source)
            script?.executeAndReturnError(nil)
            
        }
    }
    
    static func lastMessage(data: [String:Any]) throws ->RequestHandler {
        
        return {
            request, response in
            do {
                let token:String = request.header(.authorization)!
                let res = MessageManager.getLastMessage(token: token)
                let _ = try? response.setBody(json: res)
                response.completed()
                
            } catch {
                
                return
            }
            response.completed()
        }
    }
    
    
    
    
    static func uploadfile(data: [String:Any]) throws ->RequestHandler {
        
        return {
            request, response in
            var templatesPath = pathConst
            mustacheRequest(request: request, response: response, handler: Handlers.UploadHandler() as MustachePageHandler, templatePath: templatesPath.appending( "response.mustache"))
        }
    }
    
    struct UploadHandler: MustachePageHandler { // all template handlers must inherit from PageHandler
        
        func extendValuesForResponse(context contxt: MustacheWebEvaluationContext, collector: MustacheEvaluationOutputCollector) {
            #if DEBUG
                print("UploadHandler got request")
            #endif
            var values = MustacheEvaluationContext.MapType()
            // Grab the WebRequest so we can get information about what was uploaded
            let request = contxt.webRequest
            let token:String = contxt.webRequest.header(.authorization)!
            
            // create uploads dir to store files
            let fileDir = Dir(Dir.workingDir.path + "files")
            do {
                try fileDir.create()
            } catch {
                print(error)
            }
            // Grab the fileUploads array and see what's there
            // If this POST was not multi-part, then this array will be empty
            print(request)
            
            MessageManager.saveMessage(token: token, from: "client", data: "File send", dataType: "text")
            if let uploads = request.postFileUploads, uploads.count > 0 {
                
                // Create an array of dictionaries which will show what was uploaded
                // This array will be used in the corresponding mustache template
                var ary = [[String:Any]]()
                
                for upload in uploads {
                    ary.append([
                        "fieldName": upload.fieldName,
                        "contentType": upload.contentType,
                        "fileName": upload.fileName,
                        "fileSize": upload.fileSize,
                        "tmpFileName": upload.tmpFileName.length != 0 ? upload.tmpFileName : "file.file"
                        ])
                    
                    // move file to webroot
                    let thisFile = File(upload.tmpFileName)
                    do {
                        print("filename :: " + upload.fileName)
                        print("dir :: " + fileDir.path)
                        let  newFile = checkFileName(filename: upload.fileName)
                        let _ = try thisFile.moveTo(path:pathConst + newFile, overWrite: true)
                        FileLinkManager.createLink(url: newFile, token: token, mime:upload.contentType.deletingLastFilePathComponent)
                        MessageManager.saveMessage(token: token, from: "server", data: "File sended:".appending(newFile), dataType: "text")
                        
                        var res = [[String:Any]]()
                        for i in 0  ..< ary.count {
                            var obj = ary[i]
                            
                            if obj["fileName"] as? String == upload.fileName {
                                res.append([
                                    "fieldName": obj["fieldName"] ?? "",
                                    "contentType": obj["contentType"] ?? "",
                                    "fileName": newFile,
                                    "fileSize": obj["fileSize"] ?? "",
                                    "tmpFileName": obj["tmpFileName"] ?? ""
                                    ])
                            } else {
                                res.append(obj)
                            }
                        }
                        ary = res
                        print(upload.contentType.deletingLastFilePathComponent)
                    } catch {
                        print(error)
                    }
                    
                }
                values["files"] = ary
                values["count"] = ary.count
            }
            
            // Grab the regular form parameters
            let params = request.params()
            if params.count > 0 {
                // Create an array of dictionaries which will show what was posted
                // This will not include any uploaded files. Those are handled above.
                var ary = [[String:Any]]()
                
                for (name, value) in params {
                    ary.append([
                        "paramName":name,
                        "paramValue":value
                        ])
                }
                values["params"] = ary
                values["paramsCount"] = ary.count
            }
            
            values["title"] = "Upload Enumerator"
            contxt.extendValues(with: values)
            do {
                try contxt.requestCompleted(withCollector: collector)
            } catch {
                let response = contxt.webResponse
                response.status = .internalServerError
                response.appendBody(string: "\(error)")
                response.completed()
            }
        }
        
        func checkFileName(filename:String) -> String {
            var newFile = filename
            var file = File.init(pathConst + filename)
            var index:Int = 1
            while file.exists {
                var splitFile = filename.split(".")
                splitFile.remove(at: filename.split(".").count - 1)
                newFile = splitFile.joined(separator: "").appending(String(index)).appending(".").appending(filename.split(".").last!)
                file = File.init(pathConst + newFile)
                index += 1
            }
            
            return newFile
        }
    }
    
    
}
