//
//  userLogin.swift
//  staffDirectory
//
//  Created by alex borisoft on 30.04.17.
//
//

import Foundation
import CryptoSwift
import PerfectHTTP

extension Handlers {
    static func userLogin(data: [String:Any]) throws -> RequestHandler {
        return {
            request, response in
            
            let user = User()
            
            if let data = request.postBodyString {
                do {
                    // Decode the post body string as JSON
                    let obj = try data.jsonDecode() as? [String:Any]
                    
                    // Set properties
                    user.fromDict(obj ?? [String:Any]())
                    
                    if user.login.isEmpty,  user.password.isEmpty {
                        let messageResponse:MessageResponse = MessageResponse.init(code: "400", responseDescription: "Пустые  данные", dataType: "text", data: "")
                        let _ = try? response.setBody(json: messageResponse)
                        response.completed()
                    }
                    
                    let authUser = User()
                    
                    try? authUser.get()
                    try? authUser.find(["login" : user.login])
                    

                    if authUser.rows().count != 0  , authUser.rows().first?.password != user.password {
                         let messageResponse:MessageResponse = MessageResponse.init(code: "400", responseDescription: "Вы ввели не правильный пароль", dataType: "text", data: "")
                        let _ = try? response.setBody(json: messageResponse)
                        response.completed()
                        return
                    } else if authUser.rows().count != 0, authUser.rows().first?.password != user.password {
                        let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "Successful", dataType: "text", data: authUser.token)
                        let _ = try? response.setBody(json: messageResponse)
                        response.completed()
                        return;
                    }
                    
                    // Save new row
                    
                    let str = obj?["login"] as! String
                    user.token = str.md5()
                    try user.save()
                    let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "Successful", dataType: "text", data: user.token)
                    let _ = try? response.setBody(json: messageResponse)

                    response.completed()

                } catch {
                    // Return an informative error
                    Handlers.error(request, response, error: "\(error)", code: .badRequest)
                    return
                }
            } else {
                // Return error
                Handlers.error(request, response, error: "No JSON submitted", code: .badRequest)
                return
            }
            
            // Deliver successful response
        }
    }
}

