//
//  MessageHandler.swift
//  staffDirectory
//
//  Created by alex borisoft on 06.05.17.
//
//

import Foundation
import PerfectHTTP

extension Handlers {
    /// API endpoint for handling the deletion of a record
    static func message(data: [String:Any]) throws -> RequestHandler {
        return {
            request, response in
            
            if let data = request.postBodyString {
                do {
                    let obj = try data.jsonDecode() as? [String:Any]
                    let mess:String = obj?["message"] as! String
                    let token:String = request.header(.authorization)!
                    
                    MessageManager.saveMessage(token: token, from: "client", data: mess, dataType: "text")
                    
                    switch mess.lowercased() {
                    case "filelist":
                        let result:[String] = FileLinkManager.getLinks(domen: "", token: token)
                        
                        var resultString = ""
                        for var str in result {
                            resultString = resultString.appendingFormat(" - %@ \n", str)
                        }
                        resultString = resultString.length > 0 ? resultString:"Файлы еще не были загружены"
                        let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "command executed", dataType: "text", data: resultString )
                        MessageManager.saveMessage(token: token, from: "server", data: resultString, dataType: "text")
                        let _ = try response.setBody(json: messageResponse)
                        response.completed()
                    case "commands":
                        let doc:String = FileReader.init().readFile(file: "commands.txt")
                        let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "command executed", dataType: "text", data: doc)
                        MessageManager.saveMessage(token: token, from: "server", data: doc, dataType: "text")

                        let _ = try response.setBody(json: messageResponse)
                        response.completed()
                        print("commands")
                        
                    case "sleep":
                        
                        let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "command executed", dataType: "text", data: "mac os fell asleep")
                        MessageManager.saveMessage(token: token, from: "server", data: "mac os fell asleep", dataType: "text")

                        let _ = try response.setBody(json: messageResponse)
                        response.completed()
                        let source = "tell application \"System Events\" to sleep"
                        
                        let script = NSAppleScript(source: source)
                        script?.executeAndReturnError(nil)
                        
                        print("sleep")
                        
                    case "shutdown":
                        let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "command executed", dataType: "text", data: "mac os shutdowning")
                        MessageManager.saveMessage(token: token, from: "server", data: "mac os shutdowning", dataType: "text")

                        let _ = try response.setBody(json: messageResponse)
                        response.completed()
                        
                        let source = "tell application \"Finder\"\nshut down\nend tell"
                        let _ = NSAppleScript(source: source)
                         print("shutdown")
                        
                    default:
                        
                        if mess.lowercased().contains("file/") {
                            let  filename:String = mess.split("/").last!
                            
                            let  link  = FileServer()
                            do {
                                try link.select(columns: [], whereclause: "fileUrl = :1", params: [filename], orderby: [])
                                
                                if link.rows().count == 1 && link.rows().first?.token == token && link.fileUrl == filename {
                                    let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "command executed", dataType: link.mime, data: filename)
                                    MessageManager.saveMessage(token: token, from: "server", data: filename, dataType: link.mime)

                                    let _ = try response.setBody(json: messageResponse)
                                    response.completed()
                                    return
                                }
                                
                                
                            }catch {
                                let messageResponse:MessageResponse = MessageResponse.init(code: "400", responseDescription: "У  данного  пользователя  не  загружено  такого  файла", dataType: "text", data: "")
                                MessageManager.saveMessage(token: token, from: "server", data: "У  данного  пользователя  не  загружено  такого  файла", dataType: "error")

                                let _ = try response.setBody(json: messageResponse)
                                response.completed()
                                return
                            }

                            
                            let messageResponse:MessageResponse = MessageResponse.init(code: "400", responseDescription: "У  данного  пользователя  не  загружено  такого  файла", dataType: "text", data: "")
                            MessageManager.saveMessage(token: token, from: "server", data: "У  данного  пользователя  не  загружено  такого  файла", dataType: "error")
                            let _ = try response.setBody(json: messageResponse)
                            response.completed()
                            return
                        }
                        let messageResponse:MessageResponse = MessageResponse.init(code: "200", responseDescription: "command echo", dataType: "text", data: mess)
                        MessageManager.saveMessage(token: token, from: "server", data: mess, dataType: "error")
                        let _ = try response.setBody(json: messageResponse)
                        response.completed()
                        print(mess)
                        return
                    }
                    
                } catch {
                    // Return an informative error
                    Handlers.error(request, response, error: "Incorrect request", code: .badRequest)
                    return
                }
            }
            response.completed()
        }
    }
    
}

