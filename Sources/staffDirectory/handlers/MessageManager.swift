//
//  MessageManager.swift
//  staffDirectory
//
//  Created by alex borisoft on 22.05.17.
//
//

import Foundation
import PerfectHTTP


class MessageManager {
    
    class func saveMessage( token: String, from:String, data:String, dataType: String) {
        
        let query = MessageDB()
        
        do {
            try? query.find([("token" , token)])
            let  index = query.rows().count
            
            let  message = MessageDB()
            message.data = data
            message.dataType = dataType
            message.token = token
            message.fromUser = from
            message.indexMessage = index + 1
            
            try message.save()
        } catch {
            print( "error Creaate Link")
        }
    }
    
    class func getLastMessage(token: String)-> [String: Any] {
        
        let lastMessage = MessageDB()
        
        
        try? lastMessage.find([("token" , token)])
        
        let maxIndex = lastMessage.rows().count
        
        var data = [[String: Any]]()
        var temp = [MessageDB]()
        
        for obj in lastMessage.rows().reversed() {
            if temp.count < 10 {
                temp.append(obj)
            }
        }
        
        for obj  in temp.reversed() {
            data.append(obj.asDataDict())
        }
        
        
        let response = ["data":data]
        
        return response
    }
}

